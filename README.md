flow3r apps - <https://flow3r.garden/apps>
==========================================
This repository is the index of flow3r apps.  If you want to publish your app,
you are in the right place!

How to publish your app
-----------------------
1. Click this link to create a new file for your app: <https://git.flow3r.garden/flow3r/flow3r-apps/-/new/main/apps>
2. _Filename_ should be `your-app-name.toml`
3. Content of the file is the name of your repository:
   ```toml
   repo = "<your-user-handle>/<app-repo-name>"
   ```
   For example, if your repository is `https://git.flow3r.garden/rahix/flow3r-field`, the file should contain
   ```toml
   repo = "rahix/flow3r-field"
   ```
4. Set commit message to `Add <your-app-name>`.
5. Click **Commit Changes**.
6. On the next page, click **Create merge request**.
7. You're done!
